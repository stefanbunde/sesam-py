import os.path

from hashlib import pbkdf2_hmac


LOWER_CASE_LETTERS = list('abcdefghijklmnopqrstuvwxyz')
UPPER_CASE_LETTERS = list('ABCDEFGHJKLMNPQRTUVWXYZ')
DIGITS = list('0123456789')
SPECIAL_CHARS = list('#!"§$%&/()[]{}=-_+*<>;:.')


class Sesam:
    password_characters = LOWER_CASE_LETTERS + UPPER_CASE_LETTERS + DIGITS + SPECIAL_CHARS
    saltfile_path = os.path.join(os.path.expanduser('~'), '.sesam')

    def __init__(self, password_length, salt):
        self.password_length = password_length
        self._salt = salt

    @property
    def salt(self):
        if self._salt is None:
            return self._get_salt()
        return self._salt

    def _get_salt(self):
        if self._saltfile_exists():
            return self._get_salt_from_saltfile()
        else:
            return self._prompt_for_salt_and_save_to_saltfile()

    def _saltfile_exists(self):
        return os.path.exists(self.saltfile_path)

    def _get_salt_from_saltfile(self):
        with open(self.saltfile_path) as fh:
            return fh.read()

    def _prompt_for_salt_and_save_to_saltfile(self):
        self._prompt_for_salt()
        if not self._saltfile_exists():
            self._save_salt_to_saltfile()
        return self._salt

    def _prompt_for_salt(self):
        self._salt = ''
        while self._salt == '':
            self._salt = input('Enter salt: ')

    def _save_salt_to_saltfile(self):
        with open(self.saltfile_path, 'w') as fh:
            fh.write(self._salt)

    def generate_password(self, master_password, domain):
        number = int.from_bytes(self._get_hashed_bytes(master_password, domain), byteorder='big')
        password = []
        while number > 0 and len(password) < self.password_length:
            password.append(self.password_characters[number % len(self.password_characters)])
            number = number // len(self.password_characters)
        return ''.join(password)

    def _get_hashed_bytes(self, master_password, domain):
        return pbkdf2_hmac(
            hash_name='sha512',
            password=(master_password + domain).encode('utf-8'),
            salt=self.salt.encode('utf-8'),
            iterations=4096
        )
