import getpass

from argparse import ArgumentParser

from sesam.sesam import Sesam


def main():
    parser = ArgumentParser()
    parser.add_argument('-d', '--domain')
    parser.add_argument('-l', '--password-length', default=12, type=int)
    parser.add_argument('-m', '--master-password')
    parser.add_argument('-s', '--salt')
    args = parser.parse_args()

    master_password = getpass.getpass('Master password: ')

    sesam = Sesam(password_length=args.password_length, salt=args.salt)

    domain = input('Domain: ')
    while domain != '':
        print(sesam.generate_password(master_password, domain))
        print()
        domain = input('Domain: ')


if __name__ == '__main__':
    main()
