from setuptools import find_packages, setup

from sesam import __version__


setup(
    name='sesam-py',
    version=__version__,

    author='Stefan Bunde',
    author_email='stefanbunde+git@posteo.de',

    packages=find_packages(),

    extras_require={
        'testing': [
            'tox',
        ]
    },

    entry_points={
        'console_scripts': [
            'sesam=sesam.main:main',
        ],
    },
)
